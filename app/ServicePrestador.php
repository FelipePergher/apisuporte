<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePrestador extends Model
{
    protected $primaryKey = 'servico_prestador_id';
    protected $table = 'servico_prestadores';

    public function servico()
    {
        return $this->hasOne('App\Service');
    }

    public function prestador()
    {
        return $this->hasOne('App\Prestador');
    }
}
