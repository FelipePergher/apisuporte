<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InteressePrestador extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'interesse_prestador_id';
    protected $table = 'interesse_prestadores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descricao', 'preco'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function post()
    {
        return $this->hasOne('App\Post');
    }

    public function prestador()
    {
        return $this->hasOne('App\Prestador');
    }
}
