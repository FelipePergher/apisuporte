<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePrestado extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'servico_prestado_id';
    protected $table = 'servicos_prestados';

    protected $dates = ['deleted_at'];

    public function post()
    {
        return $this->hasOne('App\Post');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function prestador()
    {
        return $this->hasOne('App\Prestador');
    }

}
