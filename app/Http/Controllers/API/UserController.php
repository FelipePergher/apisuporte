<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;

use App\User;
use App\Prestador;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => 'store']);
    }

    protected $validationRules=[
        'email' => 'unique:users,email',
        'cpf' => 'unique:users,cpf',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return new UserCollection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), $this->validationRules);
        
        if ($v->fails())
        {
            return response()->json($v->errors());
        }

        $user = new User();
        $user->name = $request->input("name");
        $user->endereco = $request->input("endereco");
        $user->telefone =  $request->input("telefone");
        $user->celular =  $request->input("celular");
        $user->email =  $request->input("email");
        $user->sexo =  $request->input("sexo");
        $user->cpf = $request->input("cpf");
        $user->password = bcrypt($request->input("password"));

        if($user->save()){
            return new UserResource($user);
        } else{
            return response()->json("Algo deu errado");
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::Find($id);
        return new UserResource($user);
    }

    public function getUserByPrestadorId($prestadorId)
    {
        $userId = Prestador::Where('prestador_id',$prestadorId)->first()->user_id;
        $user = User::Find($userId);
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
