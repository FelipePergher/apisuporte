<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\ServiceResource;

use App\Service;
use App\User;
use App\Prestador;
use App\ServicePrestador;

class ServiceController extends Controller
{
    public function index()
    {
        // return new ServiceCollection(Service::all());
    }

    public function store(Request $request)
    {
        //
    }

    public function show($username)
    {
        $services = Service::all();
        foreach ($services as $service){
            $service->checked = false;
        }

        $user = User::Where('email', $username)->first();
        if($user != null){
            $prestador = Prestador::Where('user_id',$user->user_id)->first();
            if ($prestador != null){
                foreach ($services as $service){
                    $check = ServicePrestador::Where("prestador_id",$prestador->prestador_id)->where('servico_id',$service->servico_id)->first();
                    if ($check != null){
                        $service->checked = true;
                    }
                }
            }
        }

        return new ServiceCollection($services);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
