<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PrestadorCollection;
use App\Http\Resources\PrestadorResource;

use App\Prestador;
use App\User;

class PrestadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email','LIKE',$request->input("username"))->first();

        if ($user == null){
            return response()->json("Algo deu errado");
        }

        $prestador = Prestador::where('user_id','=',$user->user_id)->first();
        if ($prestador != null){
            return new PrestadorResource($prestador);
        }

        $prestador = new Prestador();
        $prestador->ativo = true;
        $prestador->user_id = $user->user_id;

        if($prestador->save()){
            return new PrestadorResource($prestador);
        } else{
            return response()->json("Algo deu errado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::where('email','LIKE',$username)->first();

        if ($user == null){
            return response()->json("Algo deu errado");
        }

        $prestador = Prestador::where('user_id','=',$user->user_id)->first();

        if ($prestador != null){
            return response()->json(true);
        } else{
            return response()->json(false);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
