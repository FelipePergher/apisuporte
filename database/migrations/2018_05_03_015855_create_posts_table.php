<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->string('descricao',250);
            $table->string('dia_semana',50);
            $table->time('hora_inicial');
            $table->time('hora_final');
            //keys
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('users');
            
            $table->unsignedInteger('servico_id');
            $table->foreign('servico_id')->references('servico_id')->on('servicos');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
