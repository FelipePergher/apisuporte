<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes', function (Blueprint $table) {
            $table->increments('avaliacao_id');
            $table->string('comentario', 250)->nullable($value = true);
            $table->integer('nota');
            //keys
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('users');

            $table->unsignedInteger('prestador_id');
            $table->foreign('prestador_id')->references('prestador_id')->on('prestadores');

            $table->unsignedInteger('servico_prestado_id');
            $table->foreign('servico_prestado_id')->references('servico_prestado_id')->on('servicos_prestados');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacoes');
    }
}
