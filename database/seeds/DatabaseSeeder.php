<?php

use Illuminate\Database\Seeder;
use App\Service;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ServicesSeeder');
        $this->call('MyPostsSeeder');
    }
}
