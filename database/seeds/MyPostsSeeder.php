<?php

use Illuminate\Database\Seeder;
use App\Post;

class MyPostsSeeder extends Seeder
{
    public function run()
    {
        Post::create([
            'descricao' => 'lala',
            'dia_semana' => 'segunda',
            'hora_inicial'=> date("h:i"),
            'hora_final'=> date("h:i"),
            'user_id'=> '1',
            'servico_id'=> '1',
            
        ]);

        Post::create([
            'descricao' => 'lala',
            'dia_semana' => 'terça',
            'hora_inicial'=> date("h:i"),
            'hora_final'=> date("h:i"),
            'user_id'=> '1',
            'servico_id'=> '2',
            
        ]);

        Post::create([
            'descricao' => 'lala',
            'dia_semana' => 'quarta || quinta',
            'hora_inicial'=> date("h:i"),
            'hora_final'=> date("h:i"),
            'user_id'=> '1',
            'servico_id'=> '3',
            
        ]);

        Post::create([
            'descricao' => 'lala',
            'dia_semana' => 'quinta',
            'hora_inicial'=> date("h:i"),
            'hora_final'=> date("h:i"),
            'user_id'=> '1',
            'servico_id'=> '4',
            
        ]);
    }
}
