<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group( function () {
    Route::apiResource('post','API\PostController');
    Route::apiResource('service','API\ServiceController');    
    Route::apiResource('servicePrestador','API\ServicePrestadorController');    
    Route::apiResource('servicePrestado','API\ServicoPrestadoController');    
    Route::apiResource('prestador','API\PrestadorController');
    Route::apiResource('interessePrestador','API\InteressePrestadorController');
    Route::get('/getPosts/{username}', 'API\PostController@getPosts')->name("post.getPosts");    
    Route::get('/getMyPosts/{username}', 'API\PostController@getMyPosts')->name("post.getMyPosts");    
    Route::get('/post/destroy/{postId}', 'API\PostController@destroy')->name("post.destroy");    
    Route::get('/interessePrestador/destroy/{interessePrestadorId}', 'API\InteressePrestadorController@destroy')->name("interessePrestador.destroy");    
    Route::post('/interesse', 'API\PostController@interesse')->name("post.interesse");    
    Route::post('/desinteresse', 'API\PostController@desinteresse')->name("post.desinteresse");  
    Route::get('/getUserByPrestadorId/{prestadorId}', 'API\UserController@getUserByPrestadorId')->name("user.getUserByPrestadorId");      
});

Route::apiResource('user','API\UserController');    